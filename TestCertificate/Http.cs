﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Diagnostics;

namespace TestCertificate
{
    public class Http
    {
        public int TimeoutMinutesRecv = 10;
        public int TimeoutMinutesSend = 2;
        public bool AllowRedirs;

        string ClientDigiCert;
        TcpClient tcpClient;
        NetworkStream stream;
        SslStream sslStream;

        Encoding encBinary = Encoding.GetEncoding(1252);

        string MultipartBoundary = "---------------------------esgboundary123";

        public Hashtable HtCookies;

        ArrayList arrMultipartFormVars;

        public string LastHttpResponseHeader = "";
        public string LastRedirUrl = "";
        public string LastPostVal = "";
        string lastReadError;
        private string RequestUrl = "";

        public bool debugSessionEnabled;
        public StringBuilder sbDebugSession;

        int redirs;

        bool cachingToDisk = false;
        int thresholdToCacheToDisk = 5120000;   // 5MB
        string cachePath = "";
        Stream cacheStream;

        public string HttpVer = "1.1";

        public bool AllowAutoCharset = true;
        public string LastHttpResponseCharset = "";

        public enum HttpVerb
        {
            GET, POST, SEARCH, CLOSE
        }

        public Http()
        {
            ClientDigiCert = "";
            HtCookies = new Hashtable();
            arrMultipartFormVars = new ArrayList();
            debugSessionEnabled = false;
            sbDebugSession = new StringBuilder("");
            lastReadError = "";
            redirs = 0;
            AllowRedirs = true;
        }

        public Http(string clientDigiCert)
        {
            ClientDigiCert = clientDigiCert;
            HtCookies = new Hashtable();
            arrMultipartFormVars = new ArrayList();
            debugSessionEnabled = false;
            sbDebugSession = new StringBuilder("");
            lastReadError = "";
            redirs = 0;
            AllowRedirs = true;
        }

        public void MultipartFormVarsAdd(string name, string val)
        {
            arrMultipartFormVars.Add(new DictionaryEntry(name, val));
        }

        public void MultipartFormVarsAddFile(string name, string filename, string content_type, string content)
        {
            arrMultipartFormVars.Add(new DictionaryEntry(name, filename + "\t" + content_type + "\t" + content));
        }

        public string Send(HttpVerb httpVerb, string url, string username, string password, string extraHeaders, string referer, string onePostVal)
        {
            string uExtraHeaders = extraHeaders.ToUpper();
            this.LastHttpResponseHeader = "";
            //this.LastRedirUrl = "";
            this.LastPostVal = "";
            this.lastReadError = "";
            this.RequestUrl = "";
            this.LastHttpResponseCharset = "";

            int pos;

            cachingToDisk = false;

            if (httpVerb == HttpVerb.CLOSE)
            {
                // close
                tcpClient = null;
                arrMultipartFormVars = new ArrayList();
                return "Y:Closed";
            }

            Uri uri;
            try
            {
                uri = new Uri(url);
            }
            catch
            {
                arrMultipartFormVars = new ArrayList();
                return "N:ERROR: Unable to parse url " + url;
            }

            this.RequestUrl = url;

            try
            {
                ServicePointManager.Expect100Continue = false;          // remove kooky expect-100 header
                ServicePointManager.SetTcpKeepAlive(false, 0, 0);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;


                Uri proxy = null;
                using (WebClient web = new System.Net.WebClient())
                {
                    web.Proxy = new WebProxy("127.0.0.1", 8888);
                    proxy = web.Proxy.GetProxy(uri);
                    //proxy = new Uri("127.0.0.1:8888");
                }
                tcpClient = new System.Net.Sockets.TcpClient(proxy.DnsSafeHost, proxy.Port);


                // 2/5/2014: added logic for up to 3 attempts. there is a 20 second "connection timeout" that is NOT configurable, so this is a workaround for that
                bool connected = false;
                for (int retry = 0; retry < 3; retry++)
                {
                    try
                    {
                        tcpClient = new TcpClient(uri.Host, uri.Port);
                        connected = true;
                        break;
                    }
                    catch (Exception exc)
                    {
                        if (!exc.Message.Contains("the connected party did not properly respond after a period of time"))
                            throw;
                    }
                }
                if (!connected)
                    throw new Exception("Unable to connect to remote server");

                tcpClient.ReceiveTimeout = TimeoutMinutesRecv * 60 * 1000;    // 10 min timeout (600000ms)
                tcpClient.SendTimeout = TimeoutMinutesSend * 60 * 1000;       //  2 min timeout (120000ms)
                tcpClient.NoDelay = true;
                tcpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, false);

                stream = tcpClient.GetStream();
                sslStream = null;
                if (url.ToLower().StartsWith("https:"))
                {
                    // SSL
                    if (ClientDigiCert.Length > 0)
                    {
                        // need to send using client digicert
                        string uClientDigiCert = ClientDigiCert.Trim(' ').ToUpper();

                        ClientCertClass clientCertClass = new ClientCertClass(uClientDigiCert);
                        System.Security.Cryptography.X509Certificates.X509Certificate clientCertificate = clientCertClass.GetCert();
                        if (clientCertificate == null)
                        {
                            // Error: client digicert not found
                            arrMultipartFormVars = new ArrayList();
                            closeStreams();
                            return "N:ERROR: Client digicert " + ClientDigiCert + " not found on server " + Environment.GetEnvironmentVariable("computername").ToUpper();
                        }
                        // client digicert found: authenticate to server
                        System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificatecollection = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
                        clientCertificatecollection.Add(clientCertificate);
                        sslStream = new SslStream(tcpClient.GetStream(), false, IgnoreCertificateErrorHandler);
                        sslStream.AuthenticateAsClient(uri.Host, clientCertificatecollection, SslProtocols.Tls, false);
                    }
                    else
                    {
                        // no digital cert required
                        sslStream = new SslStream(tcpClient.GetStream(), false, IgnoreCertificateErrorHandler);
                        try
                        {
                            sslStream.AuthenticateAsClient(uri.Host, null, SslProtocols.Tls12, false);
                        }
                        catch (Exception exc)
                        {
                            try
                            {
                                sslStream.AuthenticateAsClient(uri.Host, null, SslProtocols.Tls, false);
                            }
                            catch (Exception exc2)
                            {
                                sslStream.AuthenticateAsClient(uri.Host, null, SslProtocols.Ssl3, false);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                arrMultipartFormVars = new ArrayList();
                closeStreams();
                return "N:ERROR authenticating to " + url + (ClientDigiCert.Length > 0 ? " (with digital cert " + ClientDigiCert + ")" : "") + " " + exc.Message;
            }

            // gather cookies (if any) to send in request
            string cookiesForRequest = "";
            foreach (DictionaryEntry de in HtCookies)
            //  cookiesForRequest += de.Key.ToString() + "=" + de.Value.ToString() + "; ";
            {
                string[] tmpCookie = (de.Key + "").Split('\t');
                if (tmpCookie[0] == uri.Host)
                    cookiesForRequest += tmpCookie[1] + "=" + de.Value.ToString() + "; ";
            }
            cookiesForRequest = cookiesForRequest.TrimEnd(' ').TrimEnd(';');
            if (cookiesForRequest.Length > 0)
                cookiesForRequest = "Cookie: " + cookiesForRequest + "\r\n";


            // if we have individual multipart/form entries, extract them to post value
            if (arrMultipartFormVars.Count > 0)
            {
                onePostVal = "";
                foreach (DictionaryEntry arrPostVar in arrMultipartFormVars)
                {
                    if (!arrPostVar.Value.ToString().Contains("\t"))
                        // regular value
                        onePostVal +=
                          "--" + MultipartBoundary + "\r\n" +
                          "Content-Disposition: form-data; name=\"" + arrPostVar.Key + "\"\r\n" +
                          "\r\n" +
                          arrPostVar.Value + "\r\n";
                    else
                    {
                        // file post
                        string[] tmp = arrPostVar.Value.ToString().Split('\t');
                        onePostVal +=
                          "--" + MultipartBoundary + "\r\n" +
                          "Content-Disposition: form-data; name=\"" + arrPostVar.Key + "\"; filename=\"" + tmp[0] + "\"\r\nContent-Type: " + tmp[1] + "\r\n" +
                          "\r\n" +
                          tmp[2] + "\r\n";
                    }
                }
                onePostVal += "--" + MultipartBoundary + "--\r\n";
            }
            this.LastPostVal = onePostVal;


            // assemble the REQUEST
            string request =
              (httpVerb == HttpVerb.GET ? "GET" : (httpVerb == HttpVerb.SEARCH ? "SEARCH" : "POST")) + " " + uri.PathAndQuery + " HTTP/" + HttpVer + "\r\n" +
              (uExtraHeaders.Contains("USER-AGENT:") ? "" : "User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.48\r\n") +
              (uExtraHeaders.Contains("HOST:") ? "" : "Host: " + uri.Host + "\r\n") +
              (uExtraHeaders.Contains("CONNECTION:") ? "" : "Connection: Keep-Alive\r\n") +
              (username.Length + password.Length == 0 ? "" : "Authorization: Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password)) + "\r\n") +
              (uExtraHeaders.Contains("CACHE-CONTROL:") ? "" : "Cache-Control: no-cache\r\n") +
              (uExtraHeaders.Contains("REFERER:") ? "" : (referer.Length == 0 ? "" : "Referer: " + referer + "\r\n")) +
              extraHeaders + (extraHeaders.Length > 0 ? "\r\n" : "") +
              (onePostVal.Contains(MultipartBoundary) ? "Content-Type: multipart/form-data; boundary=" + MultipartBoundary + "\r\n" : (httpVerb == HttpVerb.POST && extraHeaders.ToLower().IndexOf("content-type") == -1 ? "Content-Type: application/x-www-form-urlencoded\r\n" : "")) +
              (httpVerb == HttpVerb.POST ? "Content-Length: " + onePostVal.Length.ToString() + "\r\n" : "") +
              cookiesForRequest +
              "\r\n" +
              onePostVal;
            Console.WriteLine(request);
            StringBuilder ResponseText = new StringBuilder();
            sbDebugSession = new StringBuilder("");
            if (debugSessionEnabled)
                sbDebugSession.Append((httpVerb == HttpVerb.GET ? "GET" : (httpVerb == HttpVerb.SEARCH ? "SEARCH" : "POST")) + " " + uri.PathAndQuery + "\r\n");

            try
            {
                // transmit the request
                Byte[] data = encBinary.GetBytes(request);
                eitherStreamWrite(data, 0, data.Length, url.ToLower().StartsWith("https:"), stream, sslStream);

                if (url.ToLower().StartsWith("https:"))
                    sslStream.Flush();
                else
                    stream.Flush();

                Byte[] bytes = new byte[32768];
                bool gotCompleteHeader = false;
                int responseContentLength = 0;
                int responseContentLengthActuallyRead = 0;
                bool isChunked = false;
                int count;
                while (true)
                {
                    string tmpPiece;
                    while (true)
                    {
                        tmpPiece = "";
                        count = 0;

                        if (responseContentLength == 0)
                        {
                            count = eitherStreamRead(bytes, 0, 32768, url.ToLower().StartsWith("https:"), stream, sslStream);
                        }
                        else
                        {
                            // if we have a "content-length" we're expecting, then wait for it (with 10 min timeout)
                            if (responseContentLengthActuallyRead < responseContentLength)
                            {
                                for (int i = 0; i < (TimeoutMinutesRecv * 60 * 10); i++)    // 6000=10 minute timeout
                                {
                                    count = eitherStreamRead(bytes, 0, 32768, url.ToLower().StartsWith("https:"), stream, sslStream);
                                    if (count != 0)
                                        break;
                                    System.Threading.Thread.Sleep(100);
                                }
                            }
                        }
                        if (count <= 0)
                            break;

                        tmpPiece = encBinary.GetString(bytes, 0, count);

                        if (debugSessionEnabled)
                            sbDebugSession.Append("\r\n***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Got these " + tmpPiece.Length.ToString() + " bytes of data:\r\n" + tmpPiece);
                        //else
                        //  sbDebugSession.Append("\r\n***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Got " + tmpPiece.Length.ToString() + " bytes of data (not shown)\r\n");
                        responseContentLengthActuallyRead += count;

                        if (gotCompleteHeader)
                            break;

                        // ...still getting header
                        this.LastHttpResponseHeader += tmpPiece;

                        gotCompleteHeader = (this.LastHttpResponseHeader.Contains("\r\n\r\n"));

                        if (gotCompleteHeader)
                        {
                            // just got complete header (and maybe part of body too)
                            this.LastHttpResponseHeader = this.LastHttpResponseHeader.Substring(0, this.LastHttpResponseHeader.IndexOf("\r\n\r\n"));    // header:   trim off everything after \r\n\r\n
                            tmpPiece = tmpPiece.Substring(tmpPiece.IndexOf("\r\n\r\n") + 4);                                                            // tmpPiece: trim off everything before \r\n\r\n


                            // filter out stupid "100 Continue" header
                            if (this.LastHttpResponseHeader.StartsWith("HTTP/") && this.LastHttpResponseHeader.IndexOf(" 100") == 8 && tmpPiece.Trim().Length == 0)   // got a 100 header with no content
                            {
                                this.LastHttpResponseHeader = "";
                                responseContentLengthActuallyRead = 0;
                                gotCompleteHeader = false;
                                continue;
                            }

                            string tmpLowerHeader = this.LastHttpResponseHeader.ToLower();

                            if (AllowAutoCharset)
                            {
                                int i = tmpLowerHeader.IndexOf("charset=");
                                if (i != -1)
                                {
                                    string tmpCharset = tmpLowerHeader.Substring(i + 8);
                                    tmpCharset = (tmpCharset + "\n").Split('\n')[0].Trim();
                                    tmpCharset = (tmpCharset + ";").Split(';')[0].Trim();

                                    if (tmpCharset == "shift_jis")
                                        LastHttpResponseCharset = tmpCharset; // keep shift_jis
                                    else if (tmpCharset == "windows-31j")
                                        LastHttpResponseCharset = "932";      // 932
                                    else if (tmpCharset == "utf-8")
                                        LastHttpResponseCharset = tmpCharset; // keep utf-8
                                    else
                                        LastHttpResponseCharset = ""; // if we don't define it above, then we don't try to decode it or it will probably crash
                                }
                            }

                            // now analyze complete header for either "content-length" or "chunked"
                            if (tmpLowerHeader.Replace(" ", "").Contains("transfer-encoding:chunked"))
                            {
                                // chunked encoding
                                isChunked = true;
                            }
                            else if (tmpLowerHeader.Contains("content-length:"))
                            {
                                string tmp = this.LastHttpResponseHeader.Substring(tmpLowerHeader.IndexOf("content-length:") + 15);
                                int len = (tmp + "\n").IndexOf("\n");
                                if (len <= 0)
                                    throw new Exception("Error parsing content-length from HTML. Expected number, but got:\r\n" + this.LastHttpResponseHeader);
                                tmp = (tmp + "\n").Substring(0, len);
                                responseContentLength = Int32.Parse(tmp.Trim('\r').Trim());

                                // add header length because we'll be including that in comparison later
                                responseContentLength += this.LastHttpResponseHeader.Length + 4;
                            }

                            break;
                        }

                        // need to continue getting header: loop back up and get some more
                    }

                    if (count <= 0)
                        break;

                    int thisChunkLengthActuallyRead = 0;

                    // filter out stupid "100 Continue" header - - - DOES THIS EVER GET INVOKED HERE??????????????????????????????????????????????????????????????????
                    if (tmpPiece.StartsWith("HTTP/") && tmpPiece.IndexOf(" 100") == 8)
                    {
                        int posDblCrLf = tmpPiece.IndexOf("\r\n\r\n");
                        if (posDblCrLf == -1)
                        {
                            // entire "100 Continue" header hasn't come through yet... wait a bit
                            for (int i = 0; i < 3000; i++)
                            {
                                if (stream.DataAvailable)
                                {
                                    count = eitherStreamRead(bytes, 0, 32768, url.ToLower().StartsWith("https:"), stream, sslStream);
                                    if (count <= 0)
                                        break;

                                    tmpPiece += encBinary.GetString(bytes, 0, count);

                                    responseContentLengthActuallyRead += count;
                                    break;
                                }
                                System.Threading.Thread.Sleep(100);
                            }
                            posDblCrLf = tmpPiece.IndexOf("\r\n\r\n");
                        }
                        if (posDblCrLf != -1)
                            tmpPiece = tmpPiece.Substring(posDblCrLf + 4);

                        if (tmpPiece.Length == 0)               // if we stripped out the "100 Continue" header, and that's all there was, start over waiting for more data
                            for (int i = 0; i < 6000; i++)
                            {
                                if (stream.DataAvailable)
                                {
                                    count = eitherStreamRead(bytes, 0, 32768, url.ToLower().StartsWith("https:"), stream, sslStream);
                                    if (count <= 0)
                                        break;

                                    tmpPiece += encBinary.GetString(bytes, 0, count);

                                    responseContentLengthActuallyRead += count;
                                    break;
                                }
                                System.Threading.Thread.Sleep(100);
                            }
                    }


                    if (isChunked && tmpPiece.Length > 0)
                    {
                        if (tmpPiece == "\r\n")
                        {
                            tmpPiece = "";    // just the end of current chunk (crlf)
                            if (debugSessionEnabled)
                                sbDebugSession.Append("***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Getting chunksize. Got end of chunk (just crlf)***\r\n");
                        }
                        else
                        {
                            int thisChunkLength = 0;
                            if (debugSessionEnabled)
                                sbDebugSession.Append("***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Getting chunksize. Parsing above chunk...***\r\n");
                            while (true)
                            {
                                tmpPiece = tmpPiece.TrimStart((char)0x0D).TrimStart((char)0x0A);
                                int len = tmpPiece.IndexOf("\n");
                                if (len <= 0)
                                {
                                    thisChunkLength = -1;
                                    break;
                                }
                                if (debugSessionEnabled)
                                    sbDebugSession.Append("  ***hex value=" + tmpPiece.Substring(0, len).Replace("\r", "").Replace("\n", "") + " about to convert to number...***\r\n");
                                thisChunkLength = Int32.Parse(tmpPiece.Substring(0, len).Replace("\r", "").Replace("\n", ""), System.Globalization.NumberStyles.HexNumber);
                                if (debugSessionEnabled)
                                    sbDebugSession.Append("  ***Converted hex to decimal: thisChunkLength=" + thisChunkLength.ToString() + "***\r\n");

                                if (thisChunkLength <= 0)
                                    break;
                                tmpPiece = tmpPiece.Substring(tmpPiece.IndexOf("\n") + 1);
                                int tmpCount = encBinary.GetByteCount(tmpPiece);
                                if (tmpCount <= thisChunkLength)
                                    break;
                                // this read took in more than just one chunk: parse them out
                                if (cachingToDisk)
                                    cacheStream.Write(encBinary.GetBytes(tmpPiece.Substring(0, thisChunkLength)), 0, thisChunkLength);
                                else
                                    ResponseText.Append(tmpPiece.Substring(0, thisChunkLength));
                                tmpPiece = tmpPiece.Substring(thisChunkLength);
                            }
                            if (thisChunkLength == 0)
                                break;

                            thisChunkLengthActuallyRead += encBinary.GetByteCount(tmpPiece);

                            if (thisChunkLength != -1)
                            {
                                // entire chunk hasn't come through yet... wait a bit
                                for (int i = 0; i < 3000; i++)
                                {
                                    count = eitherStreamRead(bytes, 0, thisChunkLength - thisChunkLengthActuallyRead, url.ToLower().StartsWith("https:"), stream, sslStream);
                                    if (count < 0)
                                        break;

                                    if (count > 0)
                                    {
                                        tmpPiece += encBinary.GetString(bytes, 0, count);

                                        responseContentLengthActuallyRead += count;
                                        thisChunkLengthActuallyRead += count;
                                        if (debugSessionEnabled)
                                            sbDebugSession.Append("***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Entire chunk hasn't come through yet. here's more (now at " + thisChunkLengthActuallyRead.ToString() + " of " + thisChunkLength.ToString() + "): " + encBinary.GetString(bytes, 0, count) + "***\r\n");
                                        if (thisChunkLengthActuallyRead >= thisChunkLength)
                                            break;
                                    }
                                }
                            }
                            if (debugSessionEnabled)
                                sbDebugSession.Append("***" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " Done with this chunk***\r\n");
                        }
                    }
                    if (count < 0)
                        break;

                    if (cachingToDisk)
                        cacheStream.Write(encBinary.GetBytes(tmpPiece), 0, tmpPiece.Length);
                    else
                    {
                        //if (LastHttpResponseCharset.Length == 0)
                        ResponseText.Append(tmpPiece);
                        //else if("123456789".Contains(LastHttpResponseCharset.Substring(0, 1)))
                        //  ResponseText.Append(Encoding.GetEncoding(int.Parse(LastHttpResponseCharset)).GetString(encBinary.GetBytes(tmpPiece))); // put back to binary bytes, then decode using proper encoding
                        //else
                        //  ResponseText.Append(Encoding.GetEncoding(LastHttpResponseCharset).GetString(encBinary.GetBytes(tmpPiece))); // put back to binary bytes, then decode using proper encoding

                        // if this response is becoming too large, switch over to disk cache method so we won't get a "System.OutOfMemoryException" error
                        if (ResponseText.Length > thresholdToCacheToDisk)
                        {
                            // write out what we have so far to cache file...
                            cachePath = CreateTempDir("http") + "http.dat";   // define new temp cache file
                            cacheStream = File.Create(cachePath);
                            cacheStream.Write(encBinary.GetBytes(ResponseText.ToString()), 0, ResponseText.Length);
                            ResponseText = null;                                      // discard stringbuilder to free mem
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            cachingToDisk = true;                                     // set flag so we will cache for rest of response
                        }
                    }

                } // while

                tcpClient.Close();
                tcpClient = null;

                if (count < 0)
                    throw new Exception(lastReadError);
            }
            catch (Exception exc)
            {
                arrMultipartFormVars = new ArrayList();
                closeStreams();
                return "N:ERROR:" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + " " + exc.Message + "\r\n" + exc.StackTrace + "\r\n" + sbDebugSession.ToString();
            }

            string retHtml;
            if (cachingToDisk)
            {
                try
                {
                    cacheStream.Flush();                                                            // commit any leftover data to disk cache file
                    cacheStream.Close();
                    cacheStream.Dispose();
                    //
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch { }

                // read entire contents of disk cache file
                retHtml = encBinary.GetString(File.ReadAllBytes(cachePath));

                // cleanup
                File.Delete(cachePath);
                Directory.Delete(cachePath.Substring(0, cachePath.LastIndexOf(@"\")), true);
            }
            else
            {
                retHtml = ResponseText.ToString();
                ResponseText = null;
            }


            if (LastHttpResponseCharset.Length > 0)
            {
                // encoding specified: put back to binary bytes, then decode using proper encoding
                if ("123456789".Contains(LastHttpResponseCharset.Substring(0, 1)))
                    // encoding specified in number
                    retHtml = Encoding.GetEncoding(int.Parse(LastHttpResponseCharset)).GetString(encBinary.GetBytes(retHtml)); // put back to binary bytes, then decode using proper encoding
                else
                    // encoding specified in string
                    retHtml = Encoding.GetEncoding(LastHttpResponseCharset).GetString(encBinary.GetBytes(retHtml)); // put back to binary bytes, then decode using proper encoding
            }


            // Look through response for any returned cookies
            pos = 0;
            while (pos != -1)
            {
                pos = this.LastHttpResponseHeader.ToUpper().IndexOf("SET-COOKIE:", pos);
                if (pos != -1)
                {
                    string tmpCookieLine = this.LastHttpResponseHeader.Substring(pos + 11) + "\r\n";
                    tmpCookieLine = tmpCookieLine.Substring(0, tmpCookieLine.IndexOf("\r\n")).Trim() + ";";
                    string[] tmpCookies = tmpCookieLine.Split(';');
                    for (int c = 0; c < tmpCookies.Length; c++)
                    {
                        string tmpCookie = tmpCookies[c].Trim();
                        if (tmpCookie.Contains("="))
                        {
                            string[] tmp = tmpCookie.Split('=');
                            if (tmp[0].ToUpper() != "PATH" && tmp[0].ToUpper() != "EXPIRES")        // don't save "path" cookie, or "expires" entry???
                            {
                                if (HtCookies.ContainsKey(uri.Host + "\t" + tmp[0]))
                                    HtCookies[uri.Host + "\t" + tmp[0]] = tmp[1];     // cookie already exists: just update value
                                else
                                    HtCookies.Add(uri.Host + "\t" + tmp[0], tmp[1]);  // cookie does not exist: add it
                            }
                        }
                    }
                    pos++;
                }
            }

            // Handle 302 Redirects (recursive)
            bool isRedirectForceGet = (this.LastHttpResponseHeader.ToLower().Contains(" 302 found\r\n") || this.LastHttpResponseHeader.ToLower().Contains(" 302 moved temporarily\r\n") || this.LastHttpResponseHeader.ToLower().Contains(" 302 object moved\r\n")
              || this.LastHttpResponseHeader.ToLower().Contains(" 303 found\r\n") || this.LastHttpResponseHeader.ToLower().Contains(" 303 moved temporarily\r\n"));
            bool isRedirectPreserveVerb = (this.LastHttpResponseHeader.ToLower().Contains(" 307 found\r\n") || this.LastHttpResponseHeader.ToLower().Contains(" 307 moved temporarily\r\n"));
            if (AllowRedirs && (isRedirectForceGet || isRedirectPreserveVerb))
            {
                int redirPos = this.LastHttpResponseHeader.IndexOf("\r\nLocation: ");
                if (redirPos != -1)
                {
                    if (++redirs > 25)
                    {
                        this.LastHttpResponseHeader = "HTTP/1.1 310 too many redirects\r\n";
                        return "N:" + retHtml;
                    }

                    string redirUrl = this.LastHttpResponseHeader.Substring(redirPos + 12) + "\r\n";
                    redirUrl = redirUrl.Substring(0, redirUrl.IndexOf("\r\n"));

                    // redirect path is relative: make it absolute
                    if (!(redirUrl.StartsWith("http:") || redirUrl.StartsWith("https:")))
                    {
                        if (redirUrl.StartsWith("/"))
                            redirUrl = uri.AbsoluteUri.Substring(0, uri.AbsoluteUri.Length - uri.PathAndQuery.Length) + redirUrl;
                        else
                            redirUrl = uri.AbsoluteUri.Substring(0, uri.AbsoluteUri.LastIndexOf("/") + 1) + redirUrl;
                    }
                    LastRedirUrl = redirUrl;

                    if (isRedirectForceGet)
                        retHtml = this.Send(HttpVerb.GET, redirUrl, username, password, extraHeaders, referer, "").Substring(2);      // force it to GET
                    else
                        retHtml = this.Send(httpVerb, redirUrl, username, password, extraHeaders, referer, onePostVal).Substring(2);  // preserve original verb (like GET or POST)
                }
            }
            else
                redirs = 0;   // not a redirect: reset redir counter

            arrMultipartFormVars = new ArrayList();
            closeStreams();
            return "Y:" + retHtml;
        }

        void closeStreams()
        {
            try
            {
                if (stream != null)
                    stream.Close();
            }
            catch { }
            try
            {
                if (sslStream != null)
                    sslStream.Close();
            }
            catch { }
        }

        public string GetCookies(string url)
        {
            // get all cookies
            string retval = "";
            foreach (DictionaryEntry de in HtCookies)
                retval += de.Key.ToString() + "=" + de.Value.ToString() + ";";
            return retval;
        }
        public string GetCookies(string url, string cookieName)
        {
            // get specific cookie
            string retval = "";
            foreach (DictionaryEntry de in HtCookies)
            {
                if (de.Key.ToString().ToUpper().Contains(cookieName.ToUpper()))
                    retval = de.Value.ToString();
            }
            return retval;
        }

        public string UrlEncode(string url)
        {
            url = System.Web.HttpUtility.UrlEncode(url);
            url = url.Replace("%2b", "%2B").Replace("%2c", "%2C").Replace("%2f", "%2F").Replace("%3a", "%3A").Replace("%3d", "%3D");
            return url;
        }

        public string RemoveTags(string html)
        {
            string retval = "";
            html = html.Trim();
            if (html.Length == 0)
                return retval;

            bool isTag = (html.Substring(0, 1) == "<");
            int pos = 0;
            while (true)
            {
                if (isTag)
                {
                    pos = html.IndexOf(">");
                    if (pos == -1)
                        break;
                    html = html.Substring(pos + 1);
                    isTag = false;
                }
                else
                {
                    pos = html.IndexOf("<");
                    if (pos == -1)
                    {
                        retval += html;
                        break;
                    }
                    retval += html.Substring(0, pos);
                    html = html.Substring(pos);
                    isTag = true;
                }
            }
            return retval.Trim();
        }

        public string GetHtmlValue(string html, string fldname)
        {
            string delim;
            string fldvalue = "";
            int i;

            for (int fldtype = 1; fldtype <= 2; fldtype++)
            {
                while (true)
                {
                    // grab next field tag
                    string temps = (fldtype == 1 ? "<INPUT" : "<SELECT");
                    i = html.ToUpper().IndexOf(temps);
                    if (i == -1)
                        break;
                    html = html.Substring(i + temps.Length);

                    i = html.ToUpper().IndexOf((fldtype == 1 ? ">" : "</SELECT>"));
                    if (i == -1)
                        break;
                    string fldtag = html.Substring(0, i);
                    html = html.Substring(i + 1);

                    // now search fldtag to see if it's the correct field
                    if (fldtag.ToUpper().Contains(" NAME=" + fldname.ToUpper() + " ") || fldtag.Replace(" ", "").ToUpper().Contains("NAME='" + fldname.ToUpper() + "'") || fldtag.Replace(" ", "").ToUpper().Contains("NAME=\"" + fldname.ToUpper() + "\"")
                      || fldtag.ToUpper().Contains(" ID=" + fldname.ToUpper() + " ") || fldtag.Replace(" ", "").ToUpper().Contains("ID='" + fldname.ToUpper() + "'") || fldtag.Replace(" ", "").ToUpper().Contains("ID=\"" + fldname.ToUpper() + "\""))
                    {
                        // this is the correct field name
                        temps = (fldtype == 1 ? " VALUE" : " SELECTED VALUE");
                        i = fldtag.ToUpper().IndexOf(temps);
                        if (i == -1 && fldtype == 2)
                        {
                            temps = " SELECTED>";
                            i = fldtag.ToUpper().IndexOf(temps);
                            if (i != -1)
                            {
                                fldtag = fldtag.Substring(0, i + 1);
                                temps = " VALUE";
                                i = fldtag.ToUpper().LastIndexOf(temps);
                            }
                        }
                        if (i != -1)
                        {
                            fldtag = fldtag.Substring(i + temps.Length).Trim(' ');   // remove value
                            if (fldtag.StartsWith("="))
                            {
                                fldtag = fldtag.Substring(1);       // remove =
                                if (fldtag.StartsWith("'"))
                                {
                                    fldtag = fldtag.Substring(1);     // remove '
                                    delim = "'";
                                }
                                else if (fldtag.StartsWith("\""))
                                {
                                    fldtag = fldtag.Substring(1);     // remove "
                                    delim = "\"";
                                }
                                else
                                    delim = " ";
                                i = fldtag.IndexOf(delim);
                                if (i != -1)
                                    fldvalue = fldtag.Substring(0, i + 1 - delim.Length).Trim(' ');
                                else
                                    fldvalue = fldtag;
                            }
                        }
                        break;
                    }
                }
            }
            return fldvalue;
        }

        int eitherStreamRead(byte[] buffer, int offset, int size, bool isHttps, NetworkStream stream, SslStream sslStream)
        {
            try
            {
                int bytes;
                size = Math.Min(32768, size);
                if (isHttps)
                    bytes = sslStream.Read(buffer, 0, size);
                else
                    bytes = stream.Read(buffer, 0, size);
                return bytes;
            }
            catch (SocketException exc)
            {
                lastReadError = "Error in http:eitherStreamRead()\r\n" + exc.Message + "\r\n" + exc.StackTrace;
                Console.WriteLine(lastReadError);
                return -1;
            }
            catch (Exception exc)
            {
                lastReadError = "Error in http:eitherStreamRead()\r\n" + exc.Message + "\r\n" + exc.StackTrace;
                Console.WriteLine(lastReadError);
                return -1;
            }
        }

        void eitherStreamWrite(byte[] data, int offset, int count, bool isHttps, NetworkStream stream, SslStream sslStream)
        {
            if (isHttps)
                sslStream.Write(data, offset, count);
            else
                stream.Write(data, offset, count);
        }

        private bool IgnoreCertificateErrorHandler(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static string CreateTempDir(string id)
        {
            string tempDir = GetTempDirName() + @"\" + GetExeName() + "_" + (id.Length == 0 ? "" : id + "_") + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + Process.GetCurrentProcess().Id.ToString() + "_" + Thread.CurrentThread.ManagedThreadId.ToString() + @"\";
            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);
            return tempDir;
        }

        public static string GetTempDirName()
        {
            return @"c:\temp";
        }

        public static string GetExeName()
        {
            return Process.GetCurrentProcess().ProcessName.Replace(".vshost", "");
        }
    }











    public class ClientCertClass
    {
        string SubjectFragment;
        System.Security.Cryptography.X509Certificates.X509Certificate ClientCertificate = null;

        public ClientCertClass(string subjectFragment)
        {
            this.SubjectFragment = subjectFragment.Trim(' ').ToUpper();


            // FIRST: get System.Security.Cryptography cert
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            store.Certificates.Find(X509FindType.FindBySubjectName, SubjectFragment, false);
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate cert in store.Certificates)
            {
                string uSubj = cert.Subject.ToUpper();
                int pos = uSubj.IndexOf(SubjectFragment);
                if (pos != -1)
                {
                    string alphanums = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    if (alphanums.IndexOf(uSubj.Substring(pos - 1, 1)) == -1 && alphanums.IndexOf(uSubj.Substring(pos + SubjectFragment.Length, 1)) == -1)
                    {
                        ClientCertificate = cert;
                        break;
                    }
                }
            }
            if (store != null)
                store.Close();


            //// SECOND: get Microsoft.Web.Services2.Security cert
            //string sto = X509CertificateStore.MyStore;
            //Microsoft.Web.Services2.Security.X509.X509CertificateStore storeWs = Microsoft.Web.Services2.Security.X509.X509CertificateStore.CurrentUserStore(sto);
            //storeWs.OpenRead();
            //Microsoft.Web.Services2.Security.X509.X509CertificateCollection certcoll = storeWs.FindCertificateBySubjectString(SubjectFragment);
            //foreach (Microsoft.Web.Services2.Security.X509.X509Certificate cert in storeWs.Certificates)
            //{
            //  string uSubj = cert.Subject.ToUpper();
            //  int pos = uSubj.IndexOf(SubjectFragment);
            //  if (pos != -1)
            //  {
            //    string alphanums = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            //    if (alphanums.IndexOf(uSubj.Substring(pos - 1, 1)) == -1 && alphanums.IndexOf(uSubj.Substring(pos + SubjectFragment.Length, 1)) == -1)
            //    {
            //      ClientCertificate = cert;
            //      break;
            //    }
            //  }
            //}
            //if (storeWs != null)
            //  storeWs.Close();


        }

        public System.Security.Cryptography.X509Certificates.X509Certificate GetCert()
        {
            return ClientCertificate;
        }

        //public Microsoft.Web.Services2.Security.X509.X509Certificate GetCertWs()
        //{
        //  return ClientCertificateWs;
        //}

        public string GetSubjectMP()
        {
            if (ClientCertificate == null)
                return "";
            string digicertSubjectMP = "";
            int pos = ClientCertificate.Subject.IndexOf("OU=MP -");
            if (pos != -1)
                digicertSubjectMP = ClientCertificate.Subject.Substring(pos + 7).Split(',')[0].Trim();
            return digicertSubjectMP;
        }

        public string GetSubjectEmployeeID()
        {
            if (ClientCertificate == null)
                return "";
            string digicertSubjectEmployeeID = "";
            int pos = ClientCertificate.Subject.IndexOf("OU=EmployeeID -");
            if (pos != -1)
                digicertSubjectEmployeeID = ClientCertificate.Subject.Substring(pos + 15).Split(',')[0].Trim();
            return digicertSubjectEmployeeID;
        }

    }
}
