﻿using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;


namespace TestCertificate
{
    /// <summary>
    /// Class for sending and receiving email
    /// </summary>

    public class eMail
    {
        private string mailServer;
        private string mailServerFailover;

        public eMail(string m_mailServer, string m_mailServerFailover)
        {
            mailServer = m_mailServer;
            mailServerFailover = m_mailServerFailover;
        }

        public bool SendEMail(string from, string to, string cc, string subject, bool htmlformat, string body, bool highpriority, string[] attachmentFullpaths)
        {
            if (this.mailServer.Length == 0)
            {
                EventLog.WriteEntry(Environment.GetCommandLineArgs()[0].Substring(Environment.GetCommandLineArgs()[0].LastIndexOf(@"\") + 1), "Could not send email, no SMTP server defined.", EventLogEntryType.Error);
                return false;
            }

            MailMessage msg = new MailMessage();
            bool retVal = false;
            try
            {
                msg.From = new MailAddress(from);
                if (to != null)
                    if (to.Length > 0)
                        msg.To.Add(to);
                if (cc != null)
                    if (cc.Length > 0)
                        msg.CC.Add(cc);
                msg.Subject = subject;
                msg.IsBodyHtml = htmlformat;
                msg.Priority = (highpriority ? MailPriority.High : MailPriority.Normal);
                msg.Body = body;
                if (attachmentFullpaths != null)
                {
                    foreach (string attPath in attachmentFullpaths)
                    {
                        // add attachments
                        try
                        {
                            Attachment att = new Attachment(attPath);
                            msg.Attachments.Add(att);
                        }
                        catch
                        {
                            msg.Body += (htmlformat ? "<BR><BR>" : "\r\n\r\n") + "ERROR: Could not add attachment " + attPath + (htmlformat ? "<BR><BR>" : "\r\n\r\n");
                        }
                    }
                }
                SmtpClient smtpClient = new SmtpClient(mailServer);
                smtpClient.Send(msg);
                retVal = true;
            }
            catch
            {
                try
                {
                    if (mailServerFailover.Length == 0)
                    {
                        EventLog.WriteEntry(Environment.GetCommandLineArgs()[0].Substring(Environment.GetCommandLineArgs()[0].LastIndexOf(@"\") + 1), "Error sending email on primary SMTP server (no failover).", EventLogEntryType.Error);
                    }
                    else
                    {
                        SmtpClient smtpClient = new SmtpClient(mailServerFailover);
                        smtpClient.Send(msg);
                        retVal = true;
                    }
                }
                catch (Exception e2)
                {
                    EventLog.WriteEntry(Environment.GetCommandLineArgs()[0].Substring(Environment.GetCommandLineArgs()[0].LastIndexOf(@"\") + 1), "Error sending email on both SMTP servers. " + e2.Message, EventLogEntryType.Error);
                }
            }
            return retVal;
        }


        /// <summary>
        /// get the mail server address
        /// </summary>
        public string MailServer
        {
            get { return this.mailServer; }
        }

        /// <summary>
        /// get the mail server failover address
        /// </summary>
        public string MailServerFailover
        {
            get { return this.mailServerFailover; }
        }
    }
}
